import { Injectable } from "@nestjs/common";
import { HttpStatus } from "@nestjs/common/enums";
import { ProtoUserRoleEnum } from "@core/contracts/auth/types/auth.types";
import { IVerifyRequest, VerifyResponse } from "@core/contracts/auth/types/rpc.types";
import { UserService } from "@domains/user/user.service";
import { TokenService } from "@infrastructure/jwt/token.service";
import { RpcException } from "@nestjs/microservices";

@Injectable()
export class MicroservicesService {
	constructor(
		private readonly tokenService: TokenService,
		private readonly userService: UserService,
	) {}

	async verify(dto: IVerifyRequest): Promise<VerifyResponse> {
		try {
			const { id: userId } = await this.tokenService.validateToken(dto.token)

			if(dto.role !== undefined) {
				const user = await this.userService.getUser(userId)
				const roles = Object.values(ProtoUserRoleEnum).filter(item => typeof item === 'string') as string[]

				if(user.role !== roles[dto.role].toLowerCase()) 
					throw new RpcException({
						message: 'Forbidden',
						status: HttpStatus.FORBIDDEN
					}) 
			}

			return {
				id: userId
			}

		} catch(e) {
			throw new RpcException({
				message: 'UNAUTHORIZED',
				status: HttpStatus.UNAUTHORIZED
			})
		}

		
	}
}