import { Controller } from "@nestjs/common";
import { GrpcMethod } from "@nestjs/microservices";
import { IVerifyRequest } from "@core/contracts/auth/types/rpc.types";
import { MicroservicesService } from "./microservices.service";

@Controller()
export class MicroservicesController {

	constructor(
		private readonly microservicesService: MicroservicesService
	) {}

	@GrpcMethod('AuthController', 'Verify')
	verify(dto: IVerifyRequest) {
		return this.microservicesService.verify(dto)
	}
}