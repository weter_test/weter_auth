import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Hash } from "@core/utils/hash";
import { TokenService } from "@infrastructure/jwt/token.service";
import { UserService } from "../user/user.service";
import { AuthDto } from "./dto/auth.dto";

@Injectable()
export class AuthService {

	constructor(
		private readonly userService: UserService,
		private readonly tokenService: TokenService
	) {}

	async login(body: AuthDto) {
		const user = await this.userService.getUserWithPassword(body.login)
		
		if(!user) throw new HttpException('User not exists', HttpStatus.NOT_FOUND)

		const passwordCompare = await Hash.compare(body.password, user.password)

		if(!passwordCompare) throw new HttpException('Incorrect login or password', HttpStatus.BAD_REQUEST)

		return this.tokenService.generateTokens({id: user.id})
	}

	async refresh(refresh: string) {
		return this.tokenService.refresh(refresh)
	}

	async register(body: AuthDto) {
		const password = await Hash.make(body.password)
		const payload = {
			...body,
			password
		}

		const newUser = await this.userService.create(payload)

		return this.tokenService.generateTokens({id: newUser.id})
	}
}