import { Module } from "@nestjs/common";
import { UserModule } from "../user/user.module";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { MicroservicesController } from "./microservices/microservices.controller";
import { MicroservicesService } from "./microservices/microservices.service";

@Module({
	imports: [UserModule],
	controllers: [AuthController, MicroservicesController],
	providers: [AuthService, MicroservicesService]
})
export class AuthModule {}