import { Body, Controller, Post } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthDto } from "./dto/auth.dto";

@Controller('/auth')
export class AuthController {

	constructor(
		private readonly authService: AuthService
	) {}

	@Post('/login')
	login(@Body() body: AuthDto) {
		return this.authService.login(body)
	}

	@Post('/refresh')
	refresh(@Body('refresh') refresh: string) {
		return this.authService.refresh(refresh)
	}

	@Post('/register')
	register(@Body() body: AuthDto) {
		return this.authService.register(body)
	}
}