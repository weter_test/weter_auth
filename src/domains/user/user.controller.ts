import { Controller, Get, Param, ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { SetRole } from '@core/decorators/setRole.decorator';
import { JwtAuthGuard } from '@core/guards/auth.guard';
import { RoleGuard } from '@core/guards/roles.guard';
import { UserRoleEnum } from './enums/userRole.enum';
import { UserService } from './user.service';

@Controller('user')
@UseGuards(JwtAuthGuard, RoleGuard)
@SetRole(UserRoleEnum.admin)
export class UserController {

	constructor(
		private readonly userService: UserService
	) {}

	@Get('list')
	getUsers() {
		return this.userService.getUsers()
	}
	
	@Get(':id')
	getUser(@Param('id', new ParseUUIDPipe({version: '4'})) id: string) {
		return this.userService.getUser(id)
	}
}