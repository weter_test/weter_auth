import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { UserRoleEnum } from "../enums/userRole.enum";

export interface IUser {
	id: string
	login: string
	role: UserRoleEnum
	password: string
}

@Entity({name: 'user'})
export class UserEntity implements IUser {

	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column({type: 'varchar', unique: true, nullable: false})
	login: string;

	@Column({type: 'enum', enum: UserRoleEnum, default: UserRoleEnum.user})
	role: UserRoleEnum
	
	@Column({type: 'varchar', nullable: false, select: false})
	password: string
}