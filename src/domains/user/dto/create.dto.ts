import { IsString, IsNotEmpty, IsEnum, IsOptional } from "class-validator"
import { IUser } from "../entities/User.entity"
import { UserRoleEnum } from "../enums/userRole.enum"

export class CreateUserDto implements Partial<Omit<IUser, 'id'>> {
	@IsString()
	@IsNotEmpty()
	login: string

	@IsString()
	@IsNotEmpty()
	password: string

	@IsEnum(UserRoleEnum)
	@IsOptional()
	role?: UserRoleEnum
}