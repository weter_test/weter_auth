import { Controller } from "@nestjs/common";
import { GrpcMethod } from "@nestjs/microservices";
import { IUserListRequest, IUserListResponse } from "@core/contracts/user/types/rpc.types";
import { MicroservicesService } from "./microservices.service";

@Controller()
export class MicroservicesController {

	constructor(
		private readonly microservicesService: MicroservicesService
	) {}

	@GrpcMethod('UserController', 'UserList')
	async userList(dto: IUserListRequest): Promise<IUserListResponse> {
		const users = await this.microservicesService.userList(dto)
		return {users: users.map(item => ({id: item.id, login: item.login}))}
	}
}