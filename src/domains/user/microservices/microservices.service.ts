import { Injectable } from "@nestjs/common";
import { IUserListRequest } from "@core/contracts/user/types/rpc.types";
import { UserService } from "@domains/user/user.service";
import { UserEntity } from "../entities/User.entity";

@Injectable()
export class MicroservicesService {
	constructor(
		private readonly userService: UserService
	) {}

	async userList(dto: IUserListRequest): Promise<UserEntity[]> {
		return this.userService.gerUserByIds(dto.users || [])
	}
}