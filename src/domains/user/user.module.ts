import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/User.entity';
import { MicroservicesController } from './microservices/microservices.controller';
import { MicroservicesService } from './microservices/microservices.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
	imports: [
		TypeOrmModule.forFeature([UserEntity])
	],
	controllers: [UserController, MicroservicesController],
	providers: [UserService, MicroservicesService],
	exports: [UserService]
})
export class UserModule {}