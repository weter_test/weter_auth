import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create.dto';
import { UserEntity } from './entities/User.entity'

@Injectable()
export class UserService {

	constructor (
		@InjectRepository(UserEntity)
		private readonly user: Repository<UserEntity>
	) {}

	getUsers(): Promise<UserEntity[]> {
		return this.user.find()
	}

	getUser(id: string): Promise<UserEntity> {
		return this.user.findOne({
			where: {id}
		})
	}

	async create(user: CreateUserDto): Promise<UserEntity> {
		const userExist = await this.user.exist({
			where: {login: user.login}
		})

		if(userExist) throw new HttpException('User exist', HttpStatus.CONFLICT)

		const newUser = this.user.create()

		this.user.merge(newUser, user)

		return this.user.save(newUser)
	}

	getUserWithPassword(login: string): Promise<UserEntity> {
		return this.user.createQueryBuilder('user')
		.addSelect('user.password')
		.where('user.login = :login', {login})
		.getOne()
	}

	gerUserByIds(ids: string[]) {
		return this.user.find({
			where: {id: In(ids)}
		})
	}
}