import { IUser } from "@domains/user/entities/User.entity";

declare global {
  namespace Express {
    interface Request {
      user: Omit<IUser, 'password'>
    }
  }
}