import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtInitModule } from './jwt/token.module';
import { PostgresModule } from './postgres/postgres.module';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    PostgresModule,
    JwtInitModule
  ],
})
export class InfrastructureModule {}
