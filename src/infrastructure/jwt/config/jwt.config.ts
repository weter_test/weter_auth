import { ConfigService } from "@nestjs/config";
import { JwtModuleOptions } from "@nestjs/jwt";

export const jwtConfigFactory = (c: ConfigService): JwtModuleOptions => {
	return {
		secretOrPrivateKey: c.getOrThrow('JWT_SECRET')
	}
}