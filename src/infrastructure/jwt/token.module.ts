import { Global, Module } from "@nestjs/common";
import { JwtModule, JwtService } from '@nestjs/jwt'
import { TokenService } from "./token.service";

@Global()
@Module({
	imports: [
		JwtModule.register({}),
	],
	providers: [TokenService, JwtService],
	exports: [TokenService, JwtService]
})
export class JwtInitModule {}