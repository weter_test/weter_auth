export interface ITokenPayload {
	id: string
	type: 'access' | 'refresh'
}

export interface ITokenGeneratePayload extends Omit<ITokenPayload, 'type'> {}