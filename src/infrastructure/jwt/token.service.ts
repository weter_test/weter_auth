import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { ITokenGeneratePayload, ITokenPayload } from "./types/tokenPayload.type";

@Injectable()
export class TokenService {
	private readonly secret = this.configService.getOrThrow('JWT_SECRET')

	constructor(
		private readonly jwtService: JwtService,
		private readonly configService: ConfigService
	) {}

	async refresh(refreshToken: string) {
		const decodeToken = await this.jwtService.verifyAsync<ITokenPayload>(refreshToken, {secret: this.secret})

		if(decodeToken.type !== 'refresh') throw new HttpException('Invalid token', HttpStatus.BAD_REQUEST)

		return this.generateTokens({id: decodeToken.id})
	}

	async generateTokens(payload: ITokenGeneratePayload) {
		const access = await this.jwtService.signAsync({...payload, type: 'access'}, {expiresIn: '1h', secret: this.secret})
		const refresh = await this.jwtService.signAsync({...payload, type: 'refresh'}, {expiresIn: '24h', secret: this.secret})

		return {
			access,
			refresh
		}
	}

	async validateToken(token: string) {
		const decodeToken = await this.jwtService.verifyAsync<ITokenPayload>(token, {secret: this.secret})

		if(decodeToken.type !== 'access') throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED)

		return decodeToken
	}
}