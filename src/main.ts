import { getAllRpcPaths } from '@core/utils/getAllRpcPaths';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { GrpcOptions } from '@nestjs/microservices';
import { Transport } from '@nestjs/microservices/enums';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('/api');

  const config = app.get(ConfigService);
  const PORT = config.getOrThrow('PORT');

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    }),
  );

  app.connectMicroservice<GrpcOptions>({
    transport: Transport.GRPC,
    options: {
      url: config.getOrThrow('MY_MICROSERVICE_URL'),
      package: ['auth_rpc', 'user_rpc'],
      protoPath: await getAllRpcPaths()
    }
  })

  await app.startAllMicroservices()

  await app.listen(PORT, () => console.log(`Auth server started on ${PORT} port`));
}

bootstrap();
