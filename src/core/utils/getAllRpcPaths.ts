import { join } from 'path';
import * as fs from 'fs';
import { promisify } from 'util';

const readdir = promisify(fs.readdir)
const stat = promisify(fs.stat)
const paths: string[] = []

export async function getAllRpcPaths(path = join(__dirname, '..', 'contracts')): Promise<string[]> {
  const result = await readdir(path)

  for(const fileOrDir of result) {
    const newPath = join(path, fileOrDir)
    const fileStat = await stat(newPath)
    if(fileStat.isDirectory()) {
      await getAllRpcPaths(newPath)
      continue;
    }

    if(fileOrDir !== 'rpc.proto') continue

    paths.push(newPath)
  }

  return paths
}