import { hash, verify } from "argon2";
import { randomBytes } from "crypto";

export class Hash {
	static make(plainText: string): Promise<string> {
		const salt = randomBytes(10)

		return hash(plainText, {salt})
	}

	static compare(plainText: string, hashText: string): Promise<boolean> {
		return verify(hashText, plainText)
	}
}