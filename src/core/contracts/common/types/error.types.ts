export interface IProtoError {
	message?: string
	status: number
}