import { Observable } from "rxjs";
import { IAuth, ProtoUserRoleEnum } from "./auth.types";

export interface IVerifyRequest {
	token: string
	role?: ProtoUserRoleEnum
}

export type VerifyResponse = IAuth

export interface IAuthController {
	verify(dto: IVerifyRequest): Observable<VerifyResponse>
}