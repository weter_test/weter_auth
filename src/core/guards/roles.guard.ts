import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Request } from "express";
import { UserRoleEnum } from "@domains/user/enums/userRole.enum";
import { ROLE_KEY } from "../decorators/setRole.decorator";

@Injectable()
export class RoleGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
    ) {}

    canActivate(ctx: ExecutionContext) {
        const role = this.reflector.getAllAndOverride<UserRoleEnum>(ROLE_KEY, [
            ctx.getHandler(),
            ctx.getClass()
        ])
        
        const req = ctx.switchToHttp().getRequest<Request>()
        console.log(role, req.user.role)
        if(role && req.user.role !== role) throw new HttpException('Permission denied', HttpStatus.FORBIDDEN)

        return true
    }
}