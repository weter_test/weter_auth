import { Inject, Injectable } from "@nestjs/common/decorators";
import { HttpStatus } from "@nestjs/common/enums";
import { HttpException } from "@nestjs/common/exceptions";
import { CanActivate, ExecutionContext } from "@nestjs/common/interfaces";
import { TokenService } from "@infrastructure/jwt/token.service";
import { UserService } from "@domains/user/user.service";
import { Request } from "express";


@Injectable()
export class JwtAuthGuard implements CanActivate {
  constructor(
		private readonly tokenService: TokenService, 
		@Inject(UserService) private readonly userService: UserService
	) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
		const request = context.switchToHttp().getRequest<Request>();
		const authHeader = request.headers.authorization;
		const [bearer, token] = authHeader.split(' ');

		if (bearer !== 'Bearer' || !token) throw new Error()

		const tokenPayload = await this.tokenService.validateToken(token);

		const user = await this.userService.getUser(tokenPayload.id)
		console.log(user, tokenPayload)
		request.user = user
		return true;
    } catch (error) {
		throw new HttpException('Unauthorized!', HttpStatus.UNAUTHORIZED)
    }
  }
}