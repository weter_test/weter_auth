import { SetMetadata } from "@nestjs/common";
import { UserRoleEnum } from "@domains/user/enums/userRole.enum";

export const ROLE_KEY = Symbol('role');
export const SetRole = (role: UserRoleEnum) => SetMetadata(ROLE_KEY, role);